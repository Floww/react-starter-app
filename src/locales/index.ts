import dataFr from "./fr.json";
import dataEn from "./en.json";

export const fr: any = dataFr;
export const en: any = dataEn;

export const locales: { [s: string]: any } = {
  en: en,
  fr: fr
};

//