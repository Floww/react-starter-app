import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { IntlProvider, addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import fr from "react-intl/locale-data/fr";
import { IAppState } from './state/reducer';

import { Provider, connect } from "react-redux";
import { store, persistor } from "./state/store";
import { PersistGate } from "redux-persist/integration/react";

import { BrowserRouter as Router } from 'react-router-dom'


addLocaleData([...en, ...fr]);

const mapStateToProps = (state: IAppState) => {
  return { locale: state.i18n.locale, messages: state.i18n.messages };
};

const ConnectedIntlProvider = connect(mapStateToProps)(IntlProvider);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedIntlProvider>
        <Router>
          <App />
        </Router>
      </ConnectedIntlProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
