/* eslint-disable */
import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import SidebarComponent from "./components/SidebarComponent";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <SidebarComponent />
                </div>
            </Router>
        );
    }
}

export default App;
