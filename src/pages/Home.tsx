import React, { Component } from 'react'
import { Layout } from 'antd';
import { FormattedMessage } from 'react-intl';

export default class Home extends Component {
    render() {
        return (
            <div>
                <h2>Home</h2>
                <Layout>
                        <FormattedMessage
                            id="home.title"
                            defaultMessage="Page acceuil"
                        />
                    </Layout>
            </div>
        )
    }
}