import { LOCALE_CHANGE } from "../reducers/i18n";

export function localeChange(locale: string) {
  return { type: LOCALE_CHANGE, payload: locale };
}

export const updateLocale = ({ dispatch }: any) => {
  return (nextLocale: string) => dispatch(localeChange(nextLocale));
};
