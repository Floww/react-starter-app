import { combineReducers } from "redux";

// import authReducer, {
//   IAuthState,
//   authPersistConfig
// } from "@state/reducers/auth";
import i18nReducer, { Ii18nState } from "./reducers/i18n";
import { persistReducer } from "redux-persist";

export interface IAppState {
  // auth: IAuthState;
  i18n: Ii18nState;
}

export default combineReducers({
  // auth: persistReducer(authPersistConfig, authReducer),
  i18n: i18nReducer //TODO: PERSIST ?
});
