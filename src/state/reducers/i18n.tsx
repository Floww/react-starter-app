import { locales, en, fr } from "../../locales/index";
import storageSession from "redux-persist/lib/storage/session";
import { persistReducer } from "redux-persist";

export const LOCALE_CHANGE = "LOCALE_CHANGE";

export const i18nPersistConfig = {
    key: "i18n",
    storage: storageSession
};

export interface Ii18nState {
    locale: string;
    messages: any;
}

const initialState: Ii18nState = {
    locale: "fr",
    messages: fr
};

function i18nReducer(state: Ii18nState = initialState, action: any) {
    const ret =
        action.type === LOCALE_CHANGE
            ? {
                locale: action.payload,
                messages: locales[action.payload] || en
            }
            : state;

    return ret;
}

export default persistReducer(i18nPersistConfig, i18nReducer);
