import React, { Component } from 'react'
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';

export default class SideBarMenuComponent extends Component {
    render() {
        return (
            <div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1">
                        <Link to={'/'}>
                            <Icon type="user" />
                            <span className="nav-text">Home</span>
                        </Link>

                    </Menu.Item>
                    <Menu.Item key="2">
                        <Link to={'/about'}>
                            <Icon type="video-camera" />
                            <span className="nav-text">About</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <Link to={'/contact'}>
                            <Icon type="upload" />
                            <span className="nav-text">Contact</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </div>
        )
    }
}
