import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom';

import Home from '../pages/Home';
import Contact from '../pages/Contact';
import About from '../pages/About';

import { Layout } from 'antd';
import SideBarMenuComponent from './SideBarMenuComponent';
const { Header, Content, Footer, Sider } = Layout;

export default class SidebarComponent extends Component {
  constructor(props: any) {
    super(props)
  }

  render() {
    return (
        <Layout style={{ height: "100vh", width: "100vw" }}>
          <Sider
            breakpoint="lg"
            collapsedWidth="0"
            onBreakpoint={broken => {
              console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
              console.log(collapsed, type);
            }}
          >
            <div className="logo" />
          <SideBarMenuComponent/>
          </Sider>
          <Layout>
            <Header style={{ background: '#fff', padding: 0 }} />
            <Content style={{ margin: '24px 16px 0' }}>
              <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                <Switch>
                  <Route exact path='/' component={Home} />
                  <Route path='/contact' component={Contact} />
                  <Route path='/about' component={About} />
                </Switch>
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
          </Layout>
        </Layout>
    )
  }
}
