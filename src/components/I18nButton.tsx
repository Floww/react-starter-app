import React, { Component } from "react";
import { connect } from "react-redux";
import { updateLocale } from "../state/actions/i18n";
import { Select } from "antd";

const { Option } = Select;

interface Props {
  locale: string;
  onLocaleChange(local: string): any;
}

class I18nButton extends Component<Props> {
  render() {
    return (
      <Select
        defaultValue={this.props.locale}
        style={{ width: 120 }}
        onChange={this.props.onLocaleChange}
      >
        <Option value="fr">Francais</Option>
        <Option value="en">English</Option>
      </Select>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    locale: state.i18n.locale
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onLocaleChange: updateLocale({ dispatch })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(I18nButton);
