const webpack = require("webpack");
const { paths } = require("react-app-rewired");
const {
 override,
//  addWebpackAlias,
 fixBabelImports,
 addLessLoader
} = require("customize-cra");
module.exports = override(
//  addWebpackAlias({
//    "@pages": ${paths.appSrc}/pages/,
//    "@assets": ${paths.appSrc}/assets/,
//    "@components": ${paths.appSrc}/components/,
//    "@state": ${paths.appSrc}/state/,
//    "@reducers": ${paths.appSrc}/state/reducers/,
//    "@actions": ${paths.appSrc}/state/actions/,
//    "@locales": ${paths.appSrc}/locales/,
//    "@models": ${paths.appSrc}/models/
//  }),
 fixBabelImports("import", {
   libraryName: "antd",
   libraryDirectory: "es",
   style: true
 }),
 addLessLoader({
   javascriptEnabled: true,
   // Customize variables
   // https://ant.design/docs/react/customize-theme
   modifyVars: {}
 })
);
